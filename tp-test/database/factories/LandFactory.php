<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Land>
 */
class LandFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $Continent = array("a"=>"Amérique", "b"=>"Europe", "c"=>"Asie", "d"=>"Afrique", "e"=>"Océanie", "f"=>"Antarctique");
        $rand_keys = array_rand($Continent, 1);

        $capitale = array("a"=>"Abidjan", "b"=>"Akra", "c"=>"Dakar", "d"=>"Lomé", "e"=>"Libreville", "f"=>"Brazaville");
        $rand_keys = array_rand($capitale, 1);

        $monnaie = array("a"=>"XOF", "b"=>"EUR", "c"=>"DOLLAR","d"=>"Francs");
        $rand_keys = array_rand($monnaie, 1);

        $language = array("a"=>"FR", "b"=>"EN", "c"=>"AR", "d"=>"ES");
        $rand_keys = array_rand($language, 1);

        //echo $Continent[$rand_keys[0]] . "\n";
        return [
            "Libelle" => $this->faker->word(),
            "description" => $this->faker->paragraph(),
            "code_indicatif" => rand(1,500),
            "Continent" => $Continent[$rand_keys],///////////
            "population" => $this->faker->country(),
            "capitale" => $this->faker->city(),///////
            "monnaie" => $monnaie[$rand_keys],
            "langue" => $language[$rand_keys],
            "Superficie" => rand(100000, 90000000),
            "est_laique" => $this->faker->boolean()
        ];
    }
}
