<?php

//LandCon.php

namespace App\Http\Controllers;

use App\Models\Land;
use Illuminate\Http\Request;
use Illuminate\Support\Str;



class LandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("lister",["lands"=>Land::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('creer');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        $request->validate([
            "capitale"=>"required"
        ]);
         //Enregistrement de land dans la bd

         $Libelle = Str::random(7);

         $description = Str::random(16);

         $Continent = array("a"=>"Amérique", "b"=>"Europe", "c"=>"Asie", "d"=>"Afrique", "e"=>"Océanie", "f"=>"Antarctique");
         $rand_keys = array_rand($Continent, 1);

         $population = array("a"=>"Côte d'Ivoire", "b"=>"Gabon", "c"=>"Niger", "d"=>"Ghana", "e"=>"Togo", "f"=>"Egytpe");
         $rand_keys = array_rand($population, 1);

         $capitale = array("a"=>"Abidjan", "b"=>"Akra", "c"=>"Dakar", "d"=>"Lomé", "e"=>"Libreville", "f"=>"Brazaville");
         $rand_keys = array_rand($capitale, 1);

         $monnaie = array("a"=>"XOF", "b"=>"EUR", "c"=>"DOLLAR","d"=>"Francs");
         $rand_keys = array_rand($monnaie, 1);

         $language = array("a"=>"FR", "b"=>"EN", "c"=>"AR", "d"=>"ES");
         $rand_keys = array_rand($language, 1);

         $est_laique = (bool) rand(0,1);
        Land::create([
            "Libelle" => $Libelle,
            "description" => $description,
            "code_indicatif" => rand(1,500),
            "Continent" => $Continent[$rand_keys],///////////
            "population" => $population[$rand_keys],
            "capitale"=>$request->get("capitale"),
            "monnaie" => $monnaie[$rand_keys],
            "langue" => $language[$rand_keys],
            "Superficie" => rand(100000, 90000000),
            "est_laique" => $est_laique
        ]);

        return redirect()->route('lister');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //$lands=Land::all();
        $land=Land::FindOrFail($id);
        return view("modifier", ["land"=>$land]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'capitale' => 'required',
            'Libelle' => 'required|min:2',
            'code_indicatif' => 'required|integer',
            'population' => 'required|min:3|max:16',
            'Superficie' => 'required|min:6|max:8',
            'Continent' => 'required',
            'monnaie' => 'required',
            'langue' => 'required',
            'est_laique' => 'required',
        ]);

        Land::whereId($id)->update($validatedData);

        return redirect()->route('lister');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $land = Land::findOrFail($id);
        $land->delete();

        return redirect()->route('lister');
    }
}
