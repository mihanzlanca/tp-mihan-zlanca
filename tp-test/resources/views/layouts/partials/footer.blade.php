<footer class="main-footer">
    <strong>Copyright &copy; 2022 <a href="https://adminlte.io">TpLaravel</a>.</strong>
    Tous les droits.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 3.1.0
    </div>
  </footer>
